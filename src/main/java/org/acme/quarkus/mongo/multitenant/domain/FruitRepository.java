package org.acme.quarkus.mongo.multitenant.domain;

import java.util.List;
import java.util.Optional;

public interface FruitRepository {

    Optional<Fruit> fetch(String id);

    void save(Fruit fruit);

    List<Fruit> list();
}
