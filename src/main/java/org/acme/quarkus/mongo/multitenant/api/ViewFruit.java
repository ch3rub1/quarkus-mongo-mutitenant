package org.acme.quarkus.mongo.multitenant.api;

import org.acme.quarkus.mongo.multitenant.domain.Fruit;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;
import java.util.Objects;

public class ViewFruit {

    private final String id;
    private final String name;
    private final String description;

    @JsonbCreator
    public ViewFruit(
        @JsonbProperty("id") String id,
        @JsonbProperty("name") String name,
        @JsonbProperty("description") String description
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public static ViewFruit fromFruit(Fruit fruit) {
        return new ViewFruit(
            fruit.getId(),
            fruit.getName(),
            fruit.getDescription()
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ViewFruit viewFruit = (ViewFruit) o;
        return Objects.equals(id, viewFruit.id) && Objects.equals(name, viewFruit.name) && Objects.equals(description, viewFruit.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
