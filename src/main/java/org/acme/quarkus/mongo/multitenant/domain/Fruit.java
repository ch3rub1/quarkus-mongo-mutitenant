package org.acme.quarkus.mongo.multitenant.domain;


import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class Fruit {

    private final String id;
    private final String name;
    private final String description;

    public Fruit(String name, String description) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.description = description;
    }

    public Fruit(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Fruit save(FruitRepository repository) {
        repository.save(this);
        return this;
    }

    public static Optional<Fruit> fetch(FruitRepository repository, String id) {
        return repository.fetch(id);
    }

    public static List<Fruit> list(FruitRepository repository) {
        return repository.list();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fruit fruit = (Fruit) o;
        return Objects.equals(id, fruit.id) && Objects.equals(name, fruit.name) && Objects.equals(description, fruit.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
