package org.acme.quarkus.mongo.multitenant.api;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.ws.rs.ConstrainedTo;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import static javax.ws.rs.RuntimeType.SERVER;

@Provider
@ConstrainedTo(SERVER)
@ApplicationScoped
public class TenantInterceptor implements ContainerRequestFilter {

    @Produces
    @RequestScoped
    Tenant tenant;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        String tenantName = requestContext.getHeaderString("X-Tenant");
        if (tenantName == null) {
            requestContext.abortWith(
                Response
                    .status(400, "Missing tenant")
                    .build()
            );
            return;
        }
        this.tenant = new Tenant(tenantName);
    }
}
