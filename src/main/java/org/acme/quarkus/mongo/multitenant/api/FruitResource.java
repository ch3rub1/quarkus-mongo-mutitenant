package org.acme.quarkus.mongo.multitenant.api;

import org.acme.quarkus.mongo.multitenant.domain.Fruit;
import org.acme.quarkus.mongo.multitenant.persistence.FruitRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/fruits")
@RequestScoped
public class FruitResource {

    private final FruitRepository repository;

    @Inject
    public FruitResource(FruitRepository repository) {
        this.repository = repository;
    }

    @GET
    @Produces(APPLICATION_JSON)
    public List<ViewFruit> list() {
        return Fruit.list(repository).stream().map(ViewFruit::fromFruit).collect(toList());
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public String add(AddFruit addFruit) {
        return addFruit.asFruit().save(repository).getId();
    }

    @GET
    @Path("/{id}")
    @Produces(APPLICATION_JSON)
    public Response find(@PathParam("id") String id) {
        return Fruit.fetch(repository, id).map(fruit -> Response.ok(fruit))
            .orElse(Response.status(404))
            .build();
    }
}