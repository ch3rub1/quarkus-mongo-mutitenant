package org.acme.quarkus.mongo.multitenant.api;

public class Tenant {

    private final String name;

    public Tenant(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
