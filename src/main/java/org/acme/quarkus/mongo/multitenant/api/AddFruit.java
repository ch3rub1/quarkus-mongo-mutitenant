package org.acme.quarkus.mongo.multitenant.api;

import org.acme.quarkus.mongo.multitenant.domain.Fruit;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

public class AddFruit {

    private final String name;
    private final String description;

    @JsonbCreator
    public AddFruit(
        @JsonbProperty("name") String name,
        @JsonbProperty("description") String description
    ) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Fruit asFruit() {
        return new Fruit(this.name, this.description);
    }
}
