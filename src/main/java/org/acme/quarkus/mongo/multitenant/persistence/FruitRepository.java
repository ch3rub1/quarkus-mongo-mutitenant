package org.acme.quarkus.mongo.multitenant.persistence;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.acme.quarkus.mongo.multitenant.domain.Fruit;
import org.jboss.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;

@RequestScoped
public class FruitRepository implements org.acme.quarkus.mongo.multitenant.domain.FruitRepository {

    private final Logger logger;
    private final MongoDatabase mongoDatabase;

    @Inject
    public FruitRepository(Logger logger, MongoDatabase mongoDatabase) {
        this.logger = logger;
        this.mongoDatabase = mongoDatabase;
    }

    public Optional<Fruit> fetch(String id) {
        return Optional.ofNullable(
            getCollection()
                .find(eq("_id", id))
                .first()
        ).map(fruitEntity -> fruitEntity.asFruit());
    }

    public void save(Fruit fruit) {
        getCollection().insertOne(FruitEntity.fromFruit(fruit));
    }

    public List<Fruit> list() {
        final List<Fruit> fruits = new ArrayList<>();
        try (MongoCursor<FruitEntity> cursor = getCollection().find().iterator()) {
            cursor.forEachRemaining(fruitEntity -> fruits.add(fruitEntity.asFruit()));
        }
        return fruits;
    }

    private MongoCollection<FruitEntity> getCollection() {
        logger.info("Using database " + mongoDatabase.getName());
        return mongoDatabase.getCollection("fruit", FruitEntity.class);
    }
}
