package org.acme.quarkus.mongo.multitenant.persistence;

import org.acme.quarkus.mongo.multitenant.domain.Fruit;
import org.bson.codecs.pojo.annotations.BsonCreator;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;

public class FruitEntity {

    private final String id;
    private final String name;
    private final String description;

    @BsonCreator
    public FruitEntity(
        @BsonId String id,
        @BsonProperty("name") String name,
        @BsonProperty("description") String description
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Fruit asFruit() {
        return new Fruit(
            this.getId(),
            this.getName(),
            this.getDescription()
        );
    }

    public static FruitEntity fromFruit(Fruit fruit) {
        return new FruitEntity(
            fruit.getId(),
            fruit.getName(),
            fruit.getDescription()
        );
    }
}
