package org.acme.quarkus.mongo.multitenant;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.quarkus.mongo.multitenant.api.AddFruit;
import org.acme.quarkus.mongo.multitenant.api.Tenant;
import org.acme.quarkus.mongo.multitenant.api.ViewFruit;
import org.acme.quarkus.mongo.multitenant.domain.Fruit;
import org.acme.quarkus.mongo.multitenant.persistence.FruitRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@QuarkusTest
@QuarkusTestResource(EmbedMongo.class)
public class FruitResourceTest {

    @InjectMock
    Tenant tenant;

    @Inject
    FruitRepository repository;

    @BeforeEach
    public void mockTenant() {
        when(tenant.getName()).thenReturn(UUID.randomUUID().toString());
    }

    @Test
    public void whenAddAFruitAndTenantMissingThenShouldRespond400() {
        given()
            .contentType(JSON)
            .body(new AddFruit("Apple", "Something"))
            .when()
            .post("/fruits")
            .then()
            .assertThat()
            .statusCode(400);
    }

    @Test
    public void whenAddAFruitThenShouldRespond200WithAddedFruitId() {
        String id = given()
            .header("X-Tenant", tenant.getName())
            .contentType(JSON)
            .body(new AddFruit("Apple", "Something"))
            .when()
            .post("/fruits")
            .then()
            .assertThat()
            .statusCode(200)
            .and()
            .extract()
            .asString();
        assertThat(repository.fetch(id).get(), is(new Fruit(id, "Apple", "Something")));
    }

    @Test
    public void whenGetAFruitAndTenantMissingThenShouldRespond400() {
        given()
            .when()
            .get("/fruits/someFruitId")
            .then()
            .assertThat()
            .statusCode(400);
    }

    @Test
    public void whenGetANonExistingFruitThenShouldRespond404() {
        given()
            .header("X-Tenant", tenant.getName())
            .when()
            .get("/fruits/nonExistingFruitId")
            .then()
            .assertThat()
            .statusCode(404);
    }

    @Test
    public void whenGetAFruitThenShouldRespond200WithFruit() {
        repository.save(new Fruit("someFruitId", "someFruitName", "someFruitDescription"));
        ViewFruit viewFruit = given()
            .header("X-Tenant", tenant.getName())
            .when()
            .get("/fruits/someFruitId")
            .then()
            .assertThat()
            .statusCode(200)
            .and()
            .extract()
            .as(ViewFruit.class);
        assertThat(viewFruit, is(new ViewFruit("someFruitId", "someFruitName", "someFruitDescription")));
    }

    @Test
    public void whenListFruitsAndTenantMissingThenShouldRespond400() {
        given()
            .when()
            .get("/fruits")
            .then()
            .assertThat()
            .statusCode(400);
    }

    @Test
    public void whenListFruitsThenShouldRespond200WithFruitList() {
        repository.save(new Fruit("someFruitId", "someFruitName", "someFruitDescription"));
        repository.save(new Fruit("anotherFruitId", "anotherFruitName", "anotherFruitDescription"));
        ViewFruit[] fruits = given()
            .header("X-Tenant", tenant.getName())
            .when()
            .get("/fruits")
            .then()
            .assertThat()
            .statusCode(200)
            .and()
            .extract()
            .as(ViewFruit[].class);
        assertThat(fruits, arrayContaining(
            new ViewFruit("someFruitId", "someFruitName", "someFruitDescription"),
            new ViewFruit("anotherFruitId", "anotherFruitName", "anotherFruitDescription")
        ));
    }
}