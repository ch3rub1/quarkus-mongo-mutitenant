package org.acme.quarkus.mongo.multitenant;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.Map;

import static de.flapdoodle.embed.mongo.distribution.Version.Main.V4_0;
import static de.flapdoodle.embed.process.runtime.Network.getFreeServerPort;
import static de.flapdoodle.embed.process.runtime.Network.localhostIsIPv6;

public class EmbedMongo implements QuarkusTestResourceLifecycleManager {

    private final int port;
    private final MongodExecutable executable;
    private MongodProcess process;

    public EmbedMongo() throws IOException {
        port = getFreeServerPort();
        IMongodConfig config = new MongodConfigBuilder().version(V4_0).net(new Net("0.0.0.0", port, localhostIsIPv6())).build();
        executable = MongodStarter.getDefaultInstance().prepare(config);
    }

    @Override
    public Map<String, String> start() {
        if (!started()) {
            try {
                process = executable.start();
            } catch (IOException exception) {
                throw new UncheckedIOException(exception);
            }
        }
        Map<String, String> quarkusConfigMap = new HashMap<>();
        quarkusConfigMap.put("quarkus.mongodb.connection-string", String.format("mongodb://localhost:%s", port));
        quarkusConfigMap.put("quarkus.mongodb.write-concern.journal", "false");
        return quarkusConfigMap;
    }

    @Override
    public void stop() {
        if (started()) {
            process.stop();
            process = null;
        }
    }

    private boolean started() {
        return process != null;
    }
}
