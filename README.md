# mongo-mutitenant project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Interact with the application

### Start mongodb

```shell script
docker run -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=admin -ti mongo:4.2
```

### Running the application in dev mode

```shell script
./mvnw compile quarkus:dev
```

### Start interacting

#### Create some fruits

```shell script
curl --verbose \
  --header "X-Tenant: myawesometenant" \
  --header "Content-Type: application/json" \
  --data '{ "name": "Apple", "description": "An apple is an edible fruit produced by an apple tree (Malus domestica). Apple trees are cultivated worldwide and are the most widely grown species in the genus Malus." }' \
  http://localhost:8080/fruits
  
curl --verbose \
  --header "X-Tenant: myawesometenant" \
  --header "Content-Type: application/json" \
  --data '{ "name": "Pear", "description": "Pears are fruits produced and consumed around the world, growing on a tree and harvested in late Summer into October." }' \
  http://localhost:8080/fruits
```

#### List all fruits

```shell script
curl --verbose \
  --header "X-Tenant: myawesometenant" \
  http://localhost:8080/fruits
```

#### Get a fruit

```shell script
curl --verbose \
  --header "X-Tenant: myawesometenant" \
  http://localhost:8080/fruits/<someid>
```